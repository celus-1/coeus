import os
import boto3
import botocore
import requests
from func_object_storage import list_files, upload_file
from flask import Flask, render_template, request, redirect, send_file
import json

BUCKET = 'flask-pdf'
UPLOAD_FOLDER = 'uploads'

S3_ENDPOINT_URL = os.environ.get('S3_ENDPOINT_URL')
S3_SERVER_PUBLIC_KEY = os.environ.get('S3_SERVER_PUBLIC_KEY')
S3_SERVER_SECRET_KEY = os.environ.get('S3_SERVER_SECRET_KEY')
S3_BUCKET_NAME = os.environ.get('S3_BUCKET_NAME')
S3_REGION = os.environ.get('S3_REGION')

BUCKET = S3_BUCKET_NAME


app = Flask(__name__)
session = boto3.session.Session()
s3 = session.client(
    service_name='s3',
    endpoint_url=S3_ENDPOINT_URL,
    region_name=S3_REGION,
    aws_access_key_id=S3_SERVER_PUBLIC_KEY,
    aws_secret_access_key=S3_SERVER_SECRET_KEY,
    config=botocore.client.Config(signature_version='s3v4'),
)


@app.context_processor
def inject_contents():
    return dict(contents=list_files(s3, BUCKET))

@app.route('/', methods=['GET', 'POST'])
def index():
    return render_template('index.html')

@app.route('/my_library')
def my_library():
    return render_template('my_library.html')

@app.route('/final_result')
def final_result():
    return render_template('final_result.html')

@app.route('/find_results')
def find_results():
    query = request.args.get('search_input')
    req = requests.get('http://parser:5000/search', params={'query' : query})
    search_result = json.loads(req.text)
    return render_template('find_results.html', search_result = search_result)

@app.route('/category')
def category():
    return render_template('category.html')


@app.route('/bookmarks')
def bookmarks():
    return render_template('bookmarks.html')

@app.route('/login')
def login():
    return render_template('login.html')

@app.route('/register')
def register():
    return render_template('register.html')

@app.route('/restore_password')
def restore_password():
    return render_template('restore_password.html')

@app.route("/storage")
def storage():
    return render_template('storage.html')

@app.route("/upload", methods=['POST'])
def upload():
    f = request.files['file']
    f.save(os.path.join(UPLOAD_FOLDER, f.filename))
    upload_file(s3, f"uploads/{f.filename}", BUCKET)

    req = requests.post('http://parser:5000/parse', data = {'fileobj_id' : f"uploads/{f.filename}"})
    return render_template('index.html')

@app.route('/search', methods=['GET'])
def search():
    query = request.args.get('query', 'text')
    req = requests.get('http://parser:5000/search', params={'query' : query})
    return req.text

@app.route('/images', methods=['GET'])
def images():
    query = request.args.get('query', 'text')
    req = requests.get('http://parser:5000/images', params={'query' : query})
    return req.text

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
