@app.route('/', methods=['GET', 'POST'])
def index():
    return render_template('index.html')

@app.route('/my_library')
def my_library():
    return render_template('my_library.html')

@app.route('/final_result')
def final_result():
    return render_template('final_result.html')

@app.route('/find_results')
def find_results():
    return render_template('find_results.html')

@app.route('/category')
def category():
    return render_template('category.html')


@app.route('/bookmarks')
def bookmarks():
    return render_template('bookmarks.html')

@app.route('/login')
def login():
    return render_template('login.html')

@app.route('/register')
def register():
    return render_template('register.html')

@app.route('/restore_password')
def restore_password():
    return render_template('restore_password.html')