def parse_pdf(s3, bucket_name, object_key):
    '''
        Input:
         - s3 - session client for object storage
         - bucket_name - identifier of a bucket where the file is stored
         - object_key - file identifier
        Output:
         - text extracted from the file
    '''
    get_object_response = s3.get_object(Bucket=bucket_name,Key=object_key)
    raw = get_object_response['Body'].read()
    content = raw
    text = content.lstrip()
    # TODO rewrite pdf parsing logic
    return text
