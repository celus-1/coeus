import os
import boto3
import fitz
import PyPDF2
import uuid
from sqlalchemy_searchable import make_searchable
from sqlalchemy_utils.types import TSVectorType
from sqlalchemy_searchable import SearchQueryMixin
from tempfile import TemporaryFile, NamedTemporaryFile
from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy, BaseQuery
from flask_migrate import Migrate
from sqlalchemy_utils.types import TSVectorType
from flask_sqlalchemy import SQLAlchemy, BaseQuery
from sqlalchemy_searchable import SearchQueryMixin
from sqlalchemy.ext.declarative import declared_attr
from flask_user import login_required, UserManager, UserMixin
from sqlalchemy.sql import exists


S3_ENDPOINT_URL = os.environ.get('S3_ENDPOINT_URL')
S3_SERVER_PUBLIC_KEY = os.environ.get('S3_SERVER_PUBLIC_KEY')
S3_SERVER_SECRET_KEY = os.environ.get('S3_SERVER_SECRET_KEY')
S3_BUCKET_NAME = os.environ.get('S3_BUCKET_NAME')
S3_REGION = os.environ.get('S3_REGION')

DATABASE_HOST=os.environ.get('DB_HOST')
DATABASE_NAME=os.environ.get('DB_DATABASE')
DATABASE_USER=os.environ.get('DB_USER')
DATABASE_PASSWORD=os.environ.get('DB_PASSWORD')

USER_APP_NAME = "Celus-1 Search Engine"
USER_ENABLE_EMAIL = False
USER_ENABLE_USERNAME = True
USER_REQUIRE_RETYPE_PASSWORD = False
app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = f"postgresql://{DATABASE_USER}:{DATABASE_PASSWORD}@{DATABASE_HOST}:5432/{DATABASE_NAME}"

session = boto3.session.Session()
s3 = session.client(
    service_name='s3',
    endpoint_url=S3_ENDPOINT_URL,
    region_name=S3_REGION,
    aws_access_key_id=S3_SERVER_PUBLIC_KEY,
    aws_secret_access_key=S3_SERVER_SECRET_KEY,
)

db = SQLAlchemy(app)
make_searchable(db.metadata)
migrate = Migrate(app, db)

class Company(db.Model):
    __tablename__ = 'company'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, unique=True)

    def __init__(self, company_name):
        self.name = company_name

class DocumentModel(db.Model):
    __tablename__ = 'document'

    id = db.Column(db.Integer, primary_key=True)
    filename = db.Column(db.String, unique=True)

    def __init__(self, filename):
        self.filename = filename

class ImageModel(db.Model):
    __tablename__ = 'images'

    id = db.Column(db.Integer(), primary_key=True)
    document_id = db.Column(db.Integer(), db.ForeignKey(DocumentModel.id))
    page_num = db.Column(db.Integer())
    image_path = db.Column(db.String())

    def __init__(self, doc_id, page_num, img):
        self.document_id = doc_id
        self.page_num = page_num
        #self.image = img
        filename = str(uuid.uuid4()) + '.' + img['ext']
        self.image_path = f"/app/static/img/{filename}"
        imgout = open(self.image_path, "wb")
        imgout.write(img["image"])

class PageQuery(BaseQuery, SearchQueryMixin):
    pass


class PageModel(db.Model):
    query_class = PageQuery
    __tablename__ = 'page'

    document_id = db.Column(db.Integer(), db.ForeignKey(DocumentModel.id), primary_key=True)
    page_num = db.Column(db.Integer(), primary_key=True)
    text = db.Column(db.UnicodeText)
    search_vector = db.Column(TSVectorType('text'))

    def __init__(self, document_id, page_num, text):
        self.document_id = document_id
        self.page_num = page_num
        self.text = text
    @declared_attr
    def __table_args__(cls):
        return (db.Index('idx_search_vector',
                cls.search_vector,
                postgresql_using="gin"),)

db.configure_mappers()
db.create_all()
db.session.commit()

def parse_pdf(s3, bucket_name, object_key):
    '''
        Input:
         - s3 - session client for object storage
         - bucket_name - identifier of a bucket where the file is stored
         - object_key - file identifier
        Output:
         - text extracted from the file
    '''
    get_object_response = s3.get_object(Bucket=bucket_name,Key=object_key)
    body = get_object_response['Body']
    tmp = TemporaryFile()
    tmp.write(body.read())
    reader = PyPDF2.PdfFileReader(tmp)
    doc = DocumentModel(object_key)
    create = not db.session.query(exists().where(DocumentModel.filename == doc.filename)).scalar()
    if create:
        db.session.add(doc)
        db.session.commit()
    res = {}
    for i, page in enumerate(reader.pages, start=1):
        text = page.extractText()
        res[i] = text
        page = PageModel(doc.id, i, text)
        if create:
            db.session.add(page)
    db.session.commit()
    return doc.id, text

def extract_images(s3, bucket_name, object_key, doc_id):
    '''
        Get an object from Object Storage, extract all image from it.
        Insert each image into database.
    '''
    get_object_response = s3.get_object(Bucket=bucket_name, Key=object_key)
    body = get_object_response['Body']
    tmp = NamedTemporaryFile(delete=False)
    tmp.write(body.read())
    tmp.close()
    doc = fitz.open(tmp.name)
    for page in range(len(doc)):
        for img in doc.getPageImageList(page):
            xref = img[0]
            pix = doc.extractImage(xref)
            image = ImageModel(doc_id, page, pix)
            db.session.add(image)
    db.session.commit()


@app.route('/', methods=['GET'])
def index():
    return 'index page'

@app.route('/parse', methods=['POST'])
def parse():
    """Get an S3-storage object id and retirieve
    a pdf connected to that id
    perform all the actions needed to parse that pdf
    return 200 on success
           error with description on failure
    """
    fileobj_id = request.form['fileobj_id']
    
    doc_id, text = parse_pdf(s3, S3_BUCKET_NAME, fileobj_id)
    extract_images(s3, S3_BUCKET_NAME, fileobj_id, doc_id)

    return text

@app.route('/search', methods=['GET'])
def search():
    query = request.args.get('query')
    pages = PageModel.query.search(query).limit(20).all()

    docs_id = set([page.document_id for page in pages])
    docs = DocumentModel.query.filter(DocumentModel.id.in_(docs_id)).all()

    res = {}
    for doc in docs:
        res[doc.filename] = {
            page.page_num : page.text for page in pages if page.document_id == doc.id
        }

    return jsonify(res)

@app.route('/images', methods=['GET'])
def images():
    query = request.args.get('query')
    pages = PageModel.query.search(query).limit(20).all()

    docs_id = set([page.document_id for page in pages])
    docs = DocumentModel.query.filter(DocumentModel.id.in_(docs_id)).all()

    res = {}
    for doc in docs:
        res[doc.filename] = {
            page.page_num : list([item.filename for item in ImageModel.query.filter(ImageModel.document_id == doc.id).filter(ImageModel.page_num == page.page_num).all()]) for page in pages if page.document_id == doc.id
        }

    return jsonify(res)


if __name__ == '__main__':
    app.run(host='0.0.0.0')
